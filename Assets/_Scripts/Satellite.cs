﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Satellite : MonoBehaviour
{
    [SerializeField]
    private GameObject mSatellite;

    [SerializeField]
    private float mSatellitesAroundSpeed;

    [SerializeField]
    [Range(6f, 10f)] float mSatellitesDistance;

    [SerializeField]
    [Range(0f, 2f)]
    float mModifierX;
    [SerializeField]
    [Range(0f, 2f)]
    float mModifierY;
    [SerializeField]
    [Range(0f, 2f)]
    float mModifierZ;

    private Vector3 mNewPos;

    [SerializeField]
    private GameObject mParentObject;

    float mCurrentAngle = 0f;
    private float mDistance;
    private float mModifierXFloat;
    private float mModifierYFloat;
    private float mModifierZFloat;

    [SerializeField] private GameObject mSphere;

    // Use this for initialization
    void Start()
    {
        mDistance = GameObject.Find("Distance").GetComponent<Scrollbar>().value;
        mModifierXFloat = GameObject.Find("Modifier X").GetComponent<Scrollbar>().value;
        mModifierYFloat = GameObject.Find("Modifier Y").GetComponent<Scrollbar>().value;
        mModifierZFloat = GameObject.Find("Modifier Z").GetComponent<Scrollbar>().value;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        TurnAround();
    }

    void Update()
    {
        //Instantiate(mSphere, transform.position, Quaternion.identity);
        CalculateDistance();
        CalculateModifier();
    }

    void TurnAround()
    {
        float PosX = transform.position.x + mParentObject.transform.position.x;
        float PosY = transform.position.y + mParentObject.transform.position.y;
        float PosZ = transform.position.z + mParentObject.transform.position.z;

        mCurrentAngle += Time.deltaTime * mSatellitesAroundSpeed;

        PosX = (float) System.Math.Cos(mCurrentAngle + mModifierX);
        PosY = (float) System.Math.Cos(mCurrentAngle + mModifierY);
        PosZ = (float) System.Math.Sin(mCurrentAngle + mModifierZ);

        transform.position = new Vector3(PosX, PosY, PosZ) * mSatellitesDistance;
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Satellite Destroyed " + collision.transform.name + transform.position);
        Destroy(gameObject);
    }

    void CalculateDistance()
    {
        mSatellitesDistance = 6 + (4*mDistance);
    }

    void CalculateModifier()
    {
        mModifierX = 0 + (2.5f * mModifierXFloat);
        mModifierY = 0 + (2.5f * mModifierYFloat);
        mModifierZ = 0 + (2.5f * mModifierZFloat);
    }
}
