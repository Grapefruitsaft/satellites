﻿using System.Collections.Generic;
using UnityEngine;

public class ShotSatellites : MonoBehaviour
{
    [SerializeField] private GameObject mSatellite;

    [SerializeField] private float mSatellites;
    
    public LayerMask mLayermask;

    private bool mSpawn;

    private Vector3 mEndSpawPoint;

    private List<GameObject> mList;

    // Use this for initialization
    void Start ()
	{
        mList = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        EndPos();
        SpawnSatellite();
    }

    void EndPos()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Input.GetMouseButtonUp(0))
        {
            if (Physics.Raycast(ray, out hit, mLayermask))
            {
                if (hit.transform.name == "Earth")
                {
                    Debug.Log("This hit at " + hit.point);
                    mEndSpawPoint = hit.point;
                    mEndSpawPoint = mEndSpawPoint * mSatellites;
                    Debug.Log(mEndSpawPoint);
                    mSpawn = true;
                }
                else
                {
                    Debug.Log("This isn't Earth");
                    mSpawn = false;
                }
            }
              
        }
    }

    void SpawnSatellite()
    { 
        if (mSpawn == true)
        {
            mList.Add((GameObject)Instantiate(mSatellite, mEndSpawPoint, Quaternion.identity)); 
            mSpawn = false;
            Debug.Log(mEndSpawPoint);
        }
    }
} // class
