﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Rotate : MonoBehaviour
{
    private Vector3 mNewPos;
    private Vector3 mVectorZero;

    private float mPosX;
    private float mPosY;

    [SerializeField] private GameObject mEarth;
    [SerializeField] private GameObject mMoon;
    

    // fictional numbers
    [SerializeField] private int mEarthSpeed;
    [SerializeField] private int mMoonSpeed;
    [SerializeField] private float mMoonTournAroundSpeed;
    

    // Use this for initialization
    void Start ()
    {
        mPosX = Mathf.Sin(Time.time);
        mPosY = Mathf.Cos(Time.time);
        mNewPos = new Vector3(mPosX, mPosY,0);

        mVectorZero = new Vector3(0,0,0);
    }
	
	// Update is called once per frame
	void Update ()
    {
	    Drehung();
        TournAround();
	}

    void Drehung()
    {
        mEarth.transform.Rotate(mNewPos * mEarthSpeed);
        mMoon.transform.Rotate(mNewPos * mMoonSpeed);
    }

    void TournAround()
    {
        mMoon.transform.RotateAround(mVectorZero, mNewPos, mMoonTournAroundSpeed);
    }
}
